import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'test',
      params: { lang: 'zh' },
      component: () => import('@/pages/index.vue')
    }
  ]
})
